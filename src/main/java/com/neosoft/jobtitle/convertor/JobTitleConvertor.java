package com.neosoft.jobtitle.convertor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.neosoft.jobtitle.dto.JobCategoryDto;
import com.neosoft.jobtitle.dto.JobSubCategoryDto;
import com.neosoft.jobtitle.entity.JobCategory;
import com.neosoft.jobtitle.entity.JobSubCategory;
import com.neosoft.jobtitle.service.JobCategoryService;


@Component
public class JobTitleConvertor {
	
	
	@Autowired
	JobCategoryService jobCategoryService;
	
	public JobCategory dtoToEntity(JobCategoryDto jobProfileDto) {
		JobCategory jobCategory=new JobCategory();
		
		try {
			
			jobCategory.setCategoryName(jobProfileDto.getCategoryName());
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return jobCategory;
		
	}
	

	public JobCategoryDto entityToDto(JobCategory jobCategory) {
		JobCategoryDto jobCategoryDto=new JobCategoryDto();
		
		try {
			
			jobCategoryDto.setCategoryName(jobCategory.getCategoryName());
			jobCategoryDto.setId(String.valueOf(jobCategory.getId()));
			List<JobSubCategoryDto> dtoList=new ArrayList<>();
			for(JobSubCategory jobSubCategory: jobCategory.getJobSubCategories())
			{
				JobSubCategoryDto categoryDto=new JobSubCategoryDto();
				categoryDto.setSubCategoryName(jobSubCategory.getSubCategoryName());
				categoryDto.setId(String.valueOf(jobSubCategory.getId()));
				dtoList.add(categoryDto);
			}
		  jobCategoryDto.setSubCategoryDtos(dtoList);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return jobCategoryDto;
		
	}


	
	public List<JobSubCategory> listDToToListEntity(List<JobSubCategoryDto> jobSubCategoryDtos,JobCategory jobCategory){
		
		List<JobSubCategory> jobSubCategories= new ArrayList<JobSubCategory>();
		try {
			for(JobSubCategoryDto jobSubCategoryDto:jobSubCategoryDtos) 
			{
				JobSubCategory jobSubCategory=dtoToEntity(jobSubCategoryDto,jobCategory);
				jobSubCategories.add(jobSubCategory);
			}
			
			
			} catch (Exception e) {
			e.printStackTrace();
			}
		return jobSubCategories;
		
	}
	
public JobSubCategory  dtoToEntity(JobSubCategoryDto jobSubCategoryDto, JobCategory jobCategory) {
		
	JobSubCategory jobSubCategory=new JobSubCategory();
	jobSubCategory.setSubCategoryName(jobSubCategoryDto.getSubCategoryName());
	jobSubCategory.setJobCategory(jobCategory);
	
	return jobSubCategory;
}


}
