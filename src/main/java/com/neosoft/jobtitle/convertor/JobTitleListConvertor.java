package com.neosoft.jobtitle.convertor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.neosoft.jobtitle.dto.JobCategoryDto;
import com.neosoft.jobtitle.dto.JobSubCategoryDto;
import com.neosoft.jobtitle.dto.JobTitleDto;
import com.neosoft.jobtitle.entity.JobAssigenCategory;
import com.neosoft.jobtitle.entity.JobAssigenSubCategory;
import com.neosoft.jobtitle.entity.JobCategory;
import com.neosoft.jobtitle.entity.JobSubCategory;
import com.neosoft.jobtitle.entity.JobTitle;
import com.neosoft.jobtitle.entity.JobTitleList;
import com.neosoft.jobtitle.repository.JobCategoryRepository;
import com.neosoft.jobtitle.repository.JobSubCategoryRepository;
import com.neosoft.jobtitle.repository.JobTitleListRepository;

@Component
public class JobTitleListConvertor {
	
	@Autowired
	JobCategoryRepository jobCategoryRepository;
	
	@Autowired
	JobSubCategoryRepository jobSubCategoryRepository;
	
	@Autowired
	JobTitleListRepository jobTitleListRepository;
	
	public JobTitle dtoToEntity(JobTitleDto jobTitleDto) {
		JobTitle jobTitle=new JobTitle();
		
		try {
			
			jobTitle.setJobTitle(jobTitleDto.getJobTitle());
			
			
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return jobTitle;
		
	}
	
public List<JobAssigenCategory> listDToToListEntity(List<JobCategoryDto> jobCategoryDtos,JobTitle jobTitle){
		
		List<JobAssigenCategory> jobAssigenCategories= new ArrayList<JobAssigenCategory>();
		try {
			for(JobCategoryDto jobCategoryDto:jobCategoryDtos)
			{
				if(jobCategoryDto.getCheckBox())
				{
					JobCategory jobCategory=jobCategoryRepository.findById(Integer.parseInt(jobCategoryDto.getId())).orElse(null);
					JobAssigenCategory jobAssigenCategory=new JobAssigenCategory();
					jobAssigenCategory.setJobCategory(jobCategory.getCategoryName());
					jobAssigenCategory.setJobTitle(jobTitle);
					List<JobAssigenSubCategory> jobAssigenSubCategories=new ArrayList<JobAssigenSubCategory>();
					for(JobSubCategoryDto jobSubCategoryDto: jobCategoryDto.getSubCategoryDtos())
					{
						if(jobSubCategoryDto.getCheckBox())
						{
						 JobAssigenSubCategory jobAssigenSubCategory=new JobAssigenSubCategory();
						 jobAssigenSubCategory.setSubCategoryName(jobSubCategoryDto.getSubCategoryName());
						 jobAssigenSubCategory.setJobTitle(jobTitle);
						 jobAssigenSubCategory.setJobAssigenCategory(jobAssigenCategory);
						 jobAssigenSubCategories.add(jobAssigenSubCategory);
						}
					}
					jobAssigenCategory.setJobAssigenSubCategories(jobAssigenSubCategories);
					jobAssigenCategories.add(jobAssigenCategory);
				}
			}
			
			
			} catch (Exception e) {
			e.printStackTrace();
			}
		return jobAssigenCategories;
		
	}


	
	

	

}
