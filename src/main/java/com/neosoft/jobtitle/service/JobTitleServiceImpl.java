package com.neosoft.jobtitle.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neosoft.jobtitle.entity.JobTitle;
import com.neosoft.jobtitle.repository.JobTitleRepository;


@Service
public class JobTitleServiceImpl implements JobTitleService{
	
	@Autowired
	JobTitleRepository jobTitleRepository;
	
	

	@Override
	public JobTitle save(JobTitle jobTitle) {
		// TODO Auto-generated method stub
		return jobTitleRepository.save(jobTitle);
	}

	
	@Override
	public Map<String, String> allJobProfile() {
		// TODO Auto-generated method stub
		Map<String, String> maps=new LinkedHashMap<>();
		List<JobTitle> jobTitles=jobTitleRepository.findAll();
		for(JobTitle jobTitle: jobTitles)
		{
			maps.put(jobTitle.getId().toString(), jobTitle.getJobTitle());
		}
		return maps;
	}

	
}
