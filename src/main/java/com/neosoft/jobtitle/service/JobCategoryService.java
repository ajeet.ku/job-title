package com.neosoft.jobtitle.service;

import org.springframework.stereotype.Service;

import com.neosoft.jobtitle.entity.JobCategory;

@Service
public interface JobCategoryService {

	JobCategory save(JobCategory jobCategory);
	
	JobCategory getById(Integer id);
	
	
}
