package com.neosoft.jobtitle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neosoft.jobtitle.entity.JobCategory;
import com.neosoft.jobtitle.repository.JobCategoryRepository;

@Service
public class JobCategoryServiceImpl implements JobCategoryService{
	
	@Autowired
	JobCategoryRepository jobCategoryRepository;

	@Override
	public JobCategory save(JobCategory jobCategory) {
		// TODO Auto-generated method stub
		return jobCategoryRepository.save(jobCategory);
	}

	@Override
	public JobCategory getById(Integer id) {
		// TODO Auto-generated method stub
		return jobCategoryRepository.findById(id).orElse(null);
	}

}
