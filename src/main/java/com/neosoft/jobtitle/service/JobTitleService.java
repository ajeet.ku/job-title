package com.neosoft.jobtitle.service;


import java.util.Map;

import org.springframework.stereotype.Service;

import com.neosoft.jobtitle.entity.JobTitle;

@Service
public interface JobTitleService {
	
	JobTitle save(JobTitle jobTitle);
	
	 Map<String, String>  allJobProfile();

}
