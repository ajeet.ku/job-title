package com.neosoft.jobtitle.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.neosoft.jobtitle.convertor.JobTitleConvertor;
import com.neosoft.jobtitle.convertor.JobTitleListConvertor;
import com.neosoft.jobtitle.dto.JobCategoryDto;
import com.neosoft.jobtitle.dto.JobSubCategoryDto;
import com.neosoft.jobtitle.dto.JobTitleDto;
import com.neosoft.jobtitle.entity.JobAssigenCategory;
import com.neosoft.jobtitle.entity.JobAssigenSubCategory;
import com.neosoft.jobtitle.entity.JobCategory;
import com.neosoft.jobtitle.entity.JobSubCategory;
import com.neosoft.jobtitle.entity.JobTitle;
import com.neosoft.jobtitle.entity.JobTitleList;
import com.neosoft.jobtitle.repository.JobAssigenCategoryRepository;
import com.neosoft.jobtitle.repository.JobAssigenSubCategoryRepository;
import com.neosoft.jobtitle.repository.JobCategoryRepository;
import com.neosoft.jobtitle.repository.JobSubCategoryRepository;
import com.neosoft.jobtitle.repository.JobTitleListRepository;
import com.neosoft.jobtitle.repository.JobTitleRepository;
import com.neosoft.jobtitle.service.JobCategoryService;
import com.neosoft.jobtitle.service.JobTitleService;


@Controller
public class JobTitleController {
	
	
private static final Logger logger  = LoggerFactory.getLogger(JobTitleController.class);
	
	@Autowired
	JobTitleConvertor jobTitleConvertor;
	
	@Autowired
	JobCategoryService jobCategoryService;
	
	@Autowired
	JobSubCategoryRepository jobSubCategoryRepository;
	
	@Autowired
	JobCategoryRepository jobCategoryRepository;

	
	@Autowired
	Gson gson;
	
	@Autowired
	JobTitleService jobProfileService;
	
	@Autowired
	JobTitleRepository jobTitleRepository;
	
	
	
	@RequestMapping(value="/job_profile", method=RequestMethod.GET)
	private ModelAndView JobProfile()
	{
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("exp-category/job-profile");
		return modelAndView;
		
	}
	
	@RequestMapping(value= "/add_job_category_master", method = RequestMethod.GET)
	public ModelAndView addJobCategoryMaster(@ModelAttribute JobCategoryDto jobCategoryDto){
		logger.info("--add_job_category_master  addJobProfile-");
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("jobCategoryDto", jobCategoryDto);
		modelAndView.setViewName("exp-category/job-category");
		
		return modelAndView;
	}
	
	@RequestMapping(value="/add_update_job_category_master", method=RequestMethod.POST)
	public ModelAndView addUpdateJobCategoryMaster(@ModelAttribute JobCategoryDto jobCategoryDto, final BindingResult result ) {
		logger.info("--  add_update_job_category_master  --");
		
		ModelAndView modelAndView = new ModelAndView();
		JobCategory jobCategory=null;
		try {
		jobCategory=jobTitleConvertor.dtoToEntity(jobCategoryDto);
		List<JobSubCategory> jobSubCategories=null;
		jobSubCategories=jobTitleConvertor.listDToToListEntity(jobCategoryDto.getSubCategoryDtos(), jobCategory);
		jobCategory.setJobSubCategories(jobSubCategories);
		jobCategoryService.save(jobCategory);
		modelAndView.addObject("token",jobCategory.getId().toString());
		modelAndView.setViewName("redirect:show_job_category_master");
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/show_job_category_master", method = RequestMethod.GET)
	public ModelAndView detailsJobCategoryMasterWithSubCategory(@ModelAttribute JobCategoryDto jobCategoryDto, @RequestParam(value="token" , required=false) String id ){
		
		logger.info("--  show_profile_category get id : "+id+" ");
		ModelAndView modelAndView = new ModelAndView();
		try {
		JobCategory jobCategory=jobCategoryService.getById(Integer.parseInt(id));
		if(jobCategory != null)
		{
			jobCategoryDto=jobTitleConvertor.entityToDto(jobCategory);
		}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		modelAndView.addObject("JobCategoryDto", jobCategoryDto);
		modelAndView.setViewName("exp-category/show_category_details");
		return modelAndView;
		
	}
	
	

	
	
	@RequestMapping(value="/job_profile_sub_category_user", method=RequestMethod.GET)
	private ModelAndView JobProfileList(@ModelAttribute JobTitleDto jobTitleDto)
	{
		ModelAndView modelAndView=new ModelAndView();
		List<JobCategory> jobCategories=jobCategoryRepository.findAll();
		if(jobCategories.size()>0)
		{
		List<JobCategoryDto> dtoList=getCategoryWiseSubCategory();
		modelAndView.addObject("categoryDtoList", dtoList);
		jobTitleDto.setJobCategoryDtos(dtoList);
		modelAndView.addObject("jobTitleDto", jobTitleDto);
		modelAndView.setViewName("exp-category/admin_job_profile");
		}else {
			modelAndView.addObject("msg", "Please Add Category");
			modelAndView.setViewName("exp-category/job-profile");
		}
		return modelAndView;
		
	}
	
	
	public List<JobCategoryDto> getCategoryWiseSubCategory()
	{
		List<JobCategoryDto> dtoList=new ArrayList<>();
		List<JobCategory> jobCategories=jobCategoryRepository.findAll();
		for(JobCategory jobCategory:jobCategories)
		{
			JobCategoryDto jobCategoryDto=new JobCategoryDto();
			jobCategoryDto.setCategoryName(jobCategory.getCategoryName());
			jobCategoryDto.setId(String.valueOf(jobCategory.getId()));
			
			List<JobSubCategoryDto> jobSubCategoryDtos=new ArrayList<>();
			for(JobSubCategory jobSubCategory:jobCategory.getJobSubCategories())
			{
				JobSubCategoryDto jobSubCategoryDto=new JobSubCategoryDto();
				jobSubCategoryDto.setSubCategoryName(jobSubCategory.getSubCategoryName());
				jobSubCategoryDto.setId(String.valueOf(jobSubCategory.getId()));
				jobSubCategoryDtos.add(jobSubCategoryDto);
			}
			jobCategoryDto.setSubCategoryDtos(jobSubCategoryDtos);
			dtoList.add(jobCategoryDto);
		}
		return dtoList;
		
	}
	
	@Autowired
	JobTitleListConvertor jobTitleListConvertor;
	
	@Autowired
	JobTitleService jobTitleService;
	
	
	@RequestMapping(value="/add_update_job_master", method=RequestMethod.POST)
	public ModelAndView addUpdateJobTitleMaster(@ModelAttribute JobTitleDto jobTitleDto, final BindingResult result ) {
		logger.info("--  add_update_job_category_master  --");
		
		ModelAndView modelAndView = new ModelAndView();
		JobTitle jobTitle=null;
		try {
		jobTitle=jobTitleListConvertor.dtoToEntity(jobTitleDto);
		List<JobAssigenCategory> jobAssigenCategories=null;
		jobAssigenCategories=jobTitleListConvertor.listDToToListEntity(jobTitleDto.getJobCategoryDtos(), jobTitle);
		jobTitle.setJobAssigenCategories(jobAssigenCategories);
		jobTitle=jobTitleService.save(jobTitle);
		modelAndView.setViewName("redirect:show_success");
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			modelAndView.setViewName("redirect:show_error");
		}
		
		return modelAndView;
	}
	
	
	@RequestMapping(value="/show_success", method=RequestMethod.GET)
	private ModelAndView JobSuccess()
	{
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("exp-category/show_success");
		return modelAndView;
		
	}
	
	@RequestMapping(value="/show_error", method=RequestMethod.GET)
	private ModelAndView JobError()
	{
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("exp-category/show_error");
		return modelAndView;
		
	}
	
	@RequestMapping(value="/job_profile_user", method=RequestMethod.GET)
	private ModelAndView JobProfileList()
	{
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("jobProfileList", jobProfileService.allJobProfile());
		modelAndView.setViewName("exp-category/user-view-profile");
		return modelAndView;
		
	}
	
	@RequestMapping(value="/job_profile_user_category", method=RequestMethod.GET)
	public ModelAndView viewCategory(@RequestParam(value="jobTitle", required=false, defaultValue="") String jobTitle){
		ModelAndView modelAndView = new ModelAndView();
		List<JobTitleDto> jobTitleDtos=new ArrayList<>();
		List<JobCategoryDto> jobCategoryDtos=new ArrayList<>();
		
		try {
			JobTitle job=jobTitleRepository.findById(Integer.parseInt(jobTitle)).orElse(null);
			if(job != null)
			{
				JobTitleDto jobTitleDto=new JobTitleDto();
				List<JobAssigenCategory> jobCategories=jobAssigenCategoryRepository.findByJobTitle(job);
				
				for(JobAssigenCategory jobCategory: jobCategories)
				{
					JobCategoryDto categoryDto=new JobCategoryDto();
					categoryDto.setCategoryName(jobCategory.getJobCategory());
					
				
				List<JobAssigenSubCategory> jobSubCategories=jobAssigenSubCategoryRepository.findByJobAssigenCategory(jobCategory);
				List<JobSubCategoryDto> jobSubCategoryDtos=new ArrayList<>();
				for(JobAssigenSubCategory jobSubCategory: jobSubCategories)
				{
					JobSubCategoryDto subCategoryDto=new JobSubCategoryDto();
					subCategoryDto.setSubCategoryName(jobSubCategory.getSubCategoryName());
					jobSubCategoryDtos.add(subCategoryDto);
				}
				categoryDto.setSubCategoryDtos(jobSubCategoryDtos);
				jobCategoryDtos.add(categoryDto);
				}
				jobTitleDto.setJobCategoryDtos(jobCategoryDtos);
				jobTitleDtos.add(jobTitleDto);	
			
			}
			modelAndView.addObject("jobTitleDtos", jobTitleDtos);
			modelAndView.setViewName("exp-category/show-user-view-profile");
				
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		return modelAndView;
		
	}
	
	@Autowired
	JobAssigenCategoryRepository jobAssigenCategoryRepository;
	
	@Autowired
	JobAssigenSubCategoryRepository jobAssigenSubCategoryRepository;
	
	
	
	@RequestMapping(value="/all_job_profile_sub_category_ajax", method=RequestMethod.GET)
	@ResponseBody
	public String getAllCategoryAndSubCategoryList(@RequestParam(value="jobTitle", required=false, defaultValue="") String jobTitle,HttpServletRequest request, HttpServletResponse response) {
		
		List<JobTitleDto> jobTitleDtos=new ArrayList<>();
		List<JobCategoryDto> jobCategoryDtos=new ArrayList<>();
		
		try {
			JobTitle job=jobTitleRepository.findById(Integer.parseInt(jobTitle)).orElse(null);
			if(job != null)
			{
				JobTitleDto jobTitleDto=new JobTitleDto();
				List<JobAssigenCategory> jobCategories=jobAssigenCategoryRepository.findByJobTitle(job);
				
				for(JobAssigenCategory jobCategory: jobCategories)
				{
					JobCategoryDto categoryDto=new JobCategoryDto();
					categoryDto.setCategoryName(jobCategory.getJobCategory());
					
					List<JobSubCategoryDto> jobSubCategoryDtos=new ArrayList<>();
				List<JobAssigenSubCategory> jobSubCategories=jobAssigenSubCategoryRepository.findByJobAssigenCategory(jobCategory);
				for(JobAssigenSubCategory jobSubCategory: jobSubCategories)
				{
					JobSubCategoryDto subCategoryDto=new JobSubCategoryDto();
					subCategoryDto.setSubCategoryName(jobSubCategory.getSubCategoryName());
					jobSubCategoryDtos.add(subCategoryDto);
				}
				categoryDto.setSubCategoryDtos(jobSubCategoryDtos);
				jobCategoryDtos.add(categoryDto);
				}
				jobTitleDto.setJobCategoryDtos(jobCategoryDtos);
				jobTitleDtos.add(jobTitleDto);	
			
			}
				return gson.toJson(jobTitleDtos);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			return "Error Occured";
		}
	}
	
	
	
	
	
	
//	@RequestMapping(value="/all_job_profile_sub_category_ajax", method=RequestMethod.GET)
//	@ResponseBody
//	public String getAllCategoryAndSubCategoryList(@RequestParam(value="jobTitle", required=false, defaultValue="") String jobTitle,HttpServletRequest request, HttpServletResponse response) {
//		
//		List<JobTitleDto> jobTitleDtos=new ArrayList<>();
//		List<JobCategoryDto> jobCategoryDtos=new ArrayList<>();
//		List<JobSubCategoryDto> jobSubCategoryDtos=new ArrayList<>();
//		try {
//			JobTitle job=jobTitleRepository.findById(Integer.parseInt(jobTitle)).orElse(null);
//			if(job != null)
//			{
//				List<JobTitleList> jobTitleLists=jobTitleListRepository.findByJobTitle(job);
//				for(JobTitleList jobTitleList: jobTitleLists)
//				{
//					
//					if(jobTitleList.getJobCategory() != null && jobTitleList.getJobSubCategory() != null)
//					{
//					JobCategory jobCategorie=jobCategoryRepository.findById(jobTitleList.getJobCategory().getId()).orElse(null);
//					
//				    JobSubCategory jobSubCategorie=jobSubCategoryRepository.findById(jobTitleList.getJobSubCategory().getId()).orElse(null);
//					
//					}
//				}
//					
//			
//			}
//				return gson.toJson(jobTitleDtos);
//		} catch (Exception e) {
//			e.printStackTrace();
//			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
//			return "Error Occured";
//		}
//	}
	
	

}
