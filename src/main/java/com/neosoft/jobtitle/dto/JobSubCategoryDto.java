package com.neosoft.jobtitle.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JobSubCategoryDto {
	
	private String id;
	
	private String subCategoryName;
	
	private Boolean checkBox;
	

}
