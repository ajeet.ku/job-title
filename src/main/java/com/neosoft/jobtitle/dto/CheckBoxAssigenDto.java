package com.neosoft.jobtitle.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CheckBoxAssigenDto {
	
	private String id;
	
	private boolean subCategoryCheckBox;

}
