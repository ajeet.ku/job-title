package com.neosoft.jobtitle.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JobCategoryDto {
	
	private String id;
	
	private String categoryName;
	
	private Boolean checkBox;
	
	private List<JobSubCategoryDto> subCategoryDtos;

}
