package com.neosoft.jobtitle.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JobTitleDto {
	
	private String id;
	private String jobTitle;
	
	//List<CheckBoxAssigenDto> checkBoxAssigenDtos;
	
	List<JobCategoryDto> jobCategoryDtos;
	
	List<JobSubCategoryDto> jobSubCategoryDtos;

}
