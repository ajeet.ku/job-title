package com.neosoft.jobtitle.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="job_assigen_sub_category")
public class JobAssigenSubCategory {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String subCategoryName;
	
	@ManyToOne
	@JoinColumn(name = "job_title")
	private JobTitle jobTitle;
	
	@ManyToOne
	@JoinColumn(name = "job_assigen_category")
	private JobAssigenCategory jobAssigenCategory;

}
