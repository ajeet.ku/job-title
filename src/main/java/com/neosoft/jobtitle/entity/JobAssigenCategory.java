package com.neosoft.jobtitle.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="job_assigen_category")
public class JobAssigenCategory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String jobCategory;
	
	@ManyToOne
	@JoinColumn(name = "job_title")
	private JobTitle jobTitle;
	
	@OneToMany(mappedBy = "jobAssigenCategory", cascade = CascadeType.ALL)
	private List<JobAssigenSubCategory> jobAssigenSubCategories;

}
