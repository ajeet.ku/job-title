package com.neosoft.jobtitle.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="job_title_list")
public class JobTitleList {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "job_title")
	private JobTitle jobTitle;
	
	@OneToOne
	@JoinColumn(name = "job_category")
	private JobCategory jobCategory;
	
	@OneToOne
	@JoinColumn(name = "job_sub_category")
	private JobSubCategory jobSubCategory;

}
