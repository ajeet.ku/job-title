package com.neosoft.jobtitle.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="job_title")
public class JobTitle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
    private String jobTitle;
    

	@OneToMany(mappedBy = "jobTitle", cascade = CascadeType.ALL)
	private List<JobTitleList> jobTitleLists;
    
	
	@OneToMany(mappedBy = "jobTitle", cascade = CascadeType.ALL)
	private List<JobAssigenCategory> jobAssigenCategories;
	
	@OneToMany(mappedBy = "jobTitle", cascade = CascadeType.ALL)
	private List<JobAssigenSubCategory> jobAssigenSubCategories;
  
   
}
