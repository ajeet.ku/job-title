package com.neosoft.jobtitle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.jobtitle.entity.JobCategory;

@Repository
public interface JobCategoryRepository extends JpaRepository<JobCategory, Integer>{

}
