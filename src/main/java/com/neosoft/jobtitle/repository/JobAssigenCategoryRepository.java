package com.neosoft.jobtitle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.jobtitle.entity.JobAssigenCategory;
import com.neosoft.jobtitle.entity.JobTitle;

@Repository
public interface JobAssigenCategoryRepository extends JpaRepository<JobAssigenCategory, Integer>{
	
	List<JobAssigenCategory> findByJobTitle(JobTitle jobTitle);
	

}
