package com.neosoft.jobtitle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.jobtitle.entity.JobTitle;

@Repository
public interface JobTitleRepository extends JpaRepository<JobTitle, Integer>{

}
