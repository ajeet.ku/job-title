package com.neosoft.jobtitle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.jobtitle.entity.JobCategory;
import com.neosoft.jobtitle.entity.JobSubCategory;

@Repository
public interface JobSubCategoryRepository extends JpaRepository<JobSubCategory, Integer>{
	
	List<JobSubCategory> findByJobCategory(JobCategory jobCategory);

	
}
