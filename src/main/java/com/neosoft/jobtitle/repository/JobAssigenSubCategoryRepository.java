package com.neosoft.jobtitle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.jobtitle.entity.JobAssigenCategory;
import com.neosoft.jobtitle.entity.JobAssigenSubCategory;

@Repository
public interface JobAssigenSubCategoryRepository extends JpaRepository<JobAssigenSubCategory, Integer>{
	
	List<JobAssigenSubCategory> findByJobAssigenCategory(JobAssigenCategory jobAssigenCategory);

}
