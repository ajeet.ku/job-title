package com.neosoft.jobtitle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.jobtitle.entity.JobTitle;
import com.neosoft.jobtitle.entity.JobTitleList;

@Repository
public interface JobTitleListRepository extends JpaRepository<JobTitleList, Integer>{
	
	List<JobTitleList>  findByJobTitle(JobTitle jobTitle);

}
