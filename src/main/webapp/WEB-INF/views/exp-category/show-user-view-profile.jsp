<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


							<div class="row">
							<c:forEach items="${jobTitleDtos}" var="job" varStatus="loop"> 
							<c:forEach items="${job.jobCategoryDtos}" var="category" varStatus="loop">
								<label > <strong>${loop.index+1 }. </strong></label>
								<label > <strong> Category Name : ${category.categoryName}</strong></label>
								<div>
								<select class="js-example-basic-single viewCategory" id="showFeedBack">
								<option value="" >Select</option>
								<option value="5" >Excellent (5 Star)</option>
								<option value="4" >Good (4 Star)</option>
								<option value="3" >Above Average (3 Star)</option>
								<option value="2" >Average (2 Star)</option>
								<option value="1" >Poor (1 Star)</option>
								<option value="0" >Very Poor (0 Star)</option>
								<option value="NA" >NA</option>
								</select>
							</div>
								<br><br>
								<c:forEach items="${category.subCategoryDtos}" var="subCategory" varStatus="loopIn"> 
								<label > Sub Category Name : ${subCategory.subCategoryName}</label><br>
								</c:forEach>
								</c:forEach>
								</c:forEach>
						</div>
						<br><br><br>
						<div  align="center" id="feedback">
						<c:forEach items="${job.jobCategoryDtos}" var="category" varStatus="loop">
								<label > <strong>${loop.index+1 }. </strong></label>
								<label > <strong> Category Name : ${category.categoryName} : </strong></label>
								</c:forEach>
						
						<!-- <div class="row"  >
						<p>
						1. Communication:
						<br>
						2. Basic Of Web Development: 
						<br>
						3. OOPs: 
						<br>
						4. Programing Language / Framework: 
						<br>
						5. Version Control: 
						<br>
						6. Database: 
						<br>
						7. Basic Concepts: 
						<br>
						8. Coding (Logical): 
						<br>
						
						--------------------------
						The Result is:
						<br> 
						Overall Grade: </p>
						</div> -->
						
						<div class="row">
						<button onclick="myFunction()">Copy To ClickBoard</button>
						</div>
						</div>
						
<script>

    function myFunction()  {
        str = document.getElementById('feedback').innerHTML;
        const el = document.createElement('textarea');
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        alert('Copied the text:' + el.value);
    };
</script>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
$(document).ready(function(){
	
	$('#feedback').hide();
	$( ".viewCategory" ).change(function() {
		$('#feedback').show();
	});
	
});
    
</script>
</body>
</html>