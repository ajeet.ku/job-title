<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin View Profile</title>

</head>
<body>
	<br>
	<br>
	<div class="row">
		
						<div  align="center">
							<label class="text-muted"><strong></strong>Job Title</label>
							<div class="row">
								<select class="js-example-basic-single profile" id="jobTitle">
								  <c:forEach items="${jobProfileList}" var="element">
									<option value="${element.key}" >${element.value}</option>
								  </c:forEach>
								   
								</select>
							</div>
							

						</div>
						


						<br>
						<br>
						<!-- <div class="row">
						 <ul id="categoryData"></ul>
						</div> -->
						 <div class="row">
						    <span id="categoryData" style="font-weight: bold;"></span>&nbsp;&nbsp;&nbsp;
						    <div>
								<select class="js-example-basic-single " id="showFeedBack">
								<option value="" >Select</option>
								<option value="5" >Excellent (5 Star)</option>
								<option value="4" >Good (4 Star)</option>
								<option value="3" >Above Average (3 Star)</option>
								<option value="2" >Average (2 Star)</option>
								<option value="1" >Poor (1 Star)</option>
								<option value="0" >Very Poor (0 Star)</option>
								<option value="NA" >NA</option>
								</select>
							</div>
						    
						    <br><br>
							<span id="subCategoryData" ></span>
						</div>
						<%-- <div class="row">
							<c:forEach items="${jobTitleDtos}" var="job" varStatus="loop"> 
							<c:forEach items="${job.jobCategoryDtos}" var="category" varStatus="loop">
								<label > <strong>${loop.index+1 }. </strong></label>
								<label > <strong> Category Name : ${category.categoryName}</strong></label>
								</c:forEach>
								</c:forEach>
						</div> --%>

				

		<br /> <br />
	</div>

	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
		$(document).ready(function() {
					/* $('.js-example-basic-single').select2(); */
					
					$('.profile').change(function(){
						var profileDate = document.getElementById("jobTitle").value;
								$.ajax({

									type : 'GET',
									url : 'all_job_profile_sub_category_ajax?jobTitle='+profileDate,
									dataType : 'json',
									type : 'GET',
									processData : false,
									contentType : false,
									statusCode : {
										404 : function() {
											alert('PAGINA NO ENCONTRADA');
										},
										500 : function() {
											alert('ERROR EN EL SERVIDOR');
										}

									},

									success : function(data) {
					
						                     
						                   /*  $.each(data, function(index, jobCategoryDtos) {
						                        $("#populateData").val(jobCategoryDtos.id).text(jobCategoryDtos.categoryName).appendTo($select);
						                    }); */ 
						                 let list=   document.getElementById("categoryData") ;
						                    let list1=   document.getElementById("subCategoryData") ;
						                    
										for (i = 0; i < data.length; i++) {
											/*   console.log(data[i]); */
											var value=data[i].jobCategoryDtos;
											console.log(value);
											  for (j = 0; j < value.length; j++) {
												  console.log(value[j].categoryName);
												  $('#showFeedBack').show();
												  let li = document.createElement("li");
											        li.innerText = value[j].categoryName;
											        list.appendChild(li);
												  var subCategory=value[j].subCategoryDtos;
													console.log(subCategory);
													
													for (k = 0; k < subCategory.length; k++) {
														console.log(subCategory[k].subCategoryName);
														/* document.getElementById("subCategoryData").innerHTML = subCategory[k].subCategoryName ; */
														let lii = document.createElement("lii");
												        lii.innerText = subCategory[k].subCategoryName;
												        list.appendChild(lii);
													}
													
												} 
											   
											} 
						                     
						              

									}
									

								});

							});
					
					$( ".viewCategory" ).change(function() {
	    	 			  if($('#jobTitle').val()!=""){
	    	 		        var viewJsp='job_profile_user_category?jobTitle='+$('#jobTitle').val();
	    	 		        window.open(viewJsp)
	    	 		    }
	    	 			  
					});
					
					$('#showFeedBack').hide();
					

				});
	</script>
	
</body>
</html>

