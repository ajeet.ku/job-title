<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

   <div class="row">
	                    <div class="col-md-12">
	                    		<h4>Category</h4>
								<div class="hr-left"></div>
								<p>${JobCategoryDto.categoryName} </p>	
			              </div>
			              </div>
			              <div class="row" >
			              <div class="col-md-6">
			              <h4>Sub Category Details :- </h4>
		
							<c:forEach items="${JobCategoryDto.subCategoryDtos}" var="category" varStatus="loop"> 
								<h5  class="control-label"><strong>${loop.index+1 }. </strong> </h5>
								<h5  class="control-label">Sub Category Name :<strong> ${category.subCategoryName}</strong></h5>
								</c:forEach>
								</div>
								</div>
								<p id="myInputF2">Ajit</p>
								<br><br>
								<button onclick="myFunction()">Copy To ClickBoard</button>
								<br><br>
								<a class="btn btn-default"  href="/add_job_category_master" role="button">Back</a>
								<br><br>
								<a class="btn btn-default"  href="/job_profile" role="button">Home</a>
								
								

	<script>

    function myFunction()  {
        str = document.getElementById('myInputF2').innerHTML;
        const el = document.createElement('textarea');
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        alert('Copied the text:' + el.value);
    };
</script>
								
</body>
</html>