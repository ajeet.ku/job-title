<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Category For Job Profile</title>
</head>
<body>
<div class="row">
<div class="col-md-12">
<div class="col-md-6">
<a class="btn btn-default"  href="/add_job_category_master" role="button">Add Category</a>
</div>
<br>
<br>
<div class="col-md-6">
<a class="btn btn-default " align="left"  href="/job_profile_sub_category_user" role="button">Add Profile</a>
</div>
<br>
<br>

<div class="col-md-6">
<a class="btn btn-default " align="left"  href="/job_profile_user" role="button">User Profile</a>
</div>

</div>
</div>

<script type="text/javascript">

 msgm='${msg}';
if(msgm != ''){
	   swal("Please Add Category ", msgm, "warning");
 }

</script>
</body>
</html>