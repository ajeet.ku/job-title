<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Category With Sub Category</title>
</head>
<body>
<div class="content-body">
      <div class="content-wrapper">
 <div class="content-header row">
          <div class="content-header-left col-md-8 col-xs-12">
         
            
          </div>
          
        </div>
		
		<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            
	            <div class="card-body collapse in">
	                <div class="card-block">
							
						
					
	                    <form:form class="form" method="POST" action="${context}/add_update_job_master" modelAttribute="jobTitleDto">
	                     
	                     
	                       
	                    <div class="form-body">
	                    <div class="row">
	                    			<div class="col-md-6 ">
			                            <p  class="control-label " >Job Title</p>
			                            
			                             <form:input maxlength="200" type="text" path="jobTitle" class="form-control" required="true" placeholder="Job Title"/>
			                            
			                        </div>
			                        </div>
			                        <br><br>
			                        <div class="row">
			                        <div class="col-md-6 ">
			                        <h4>Category Details :- </h4>
		
								<c:forEach items="${categoryDtoList}" var="category" varStatus="loop"> 
								<label > <strong>${loop.index+1 }. </strong></label>
								 <form:hidden path="jobCategoryDtos[${loop.index}].id"/>
								  <form:checkbox path="jobCategoryDtos[${loop.index}].checkBox" />
								<label > <strong> Category Name : ${category.categoryName}</strong></label>
								<br><br>
								<c:forEach items="${category.subCategoryDtos}" var="subCategory" varStatus="loopIn"> 
								 <form:hidden path="jobCategoryDtos[${loop.index}].subCategoryDtos[${loopIn.index}].id"/>
								  <form:hidden path="jobCategoryDtos[${loop.index}].subCategoryDtos[${loopIn.index}].subCategoryName"/>
								  <form:checkbox path="jobCategoryDtos[${loop.index}].subCategoryDtos[${loopIn.index}].checkBox" />
  								  <label > Sub Category Name : ${subCategory.subCategoryName}</label><br>
							<%-- 	<h4  >Sub Category Name : ${subCategory.subCategoryName}</h4> --%>
								</c:forEach>
								<br><br>
								</c:forEach>
			                        </div>
			                        </div>
	                   
	                    
	                      </div>
							
							 <div class="form-actions right">
	                          
	                            
	                            
	                             <input type="button" class="btn btn btn-primary " id="saveChanges" value="Next" />
	                             
	                           
	                        </div>
	               <br><br>
								<br><br>
								<a class="btn btn-default"  href="/job_profile" role="button">Home</a>
                        
	                    </form:form>
	                </div>
	            </div>
	        </div>
	    </div>
</div>
</div>
	  
	</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
var counter=$('#counter').val();
$(document).ready(function(){
	 $("#saveChanges").click(function(){
			
			$("#jobTitleDto").submit();
			
			});
	
});
</script>			
</body>
</html>