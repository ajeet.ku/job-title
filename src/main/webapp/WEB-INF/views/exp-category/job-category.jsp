<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Category With Sub Category</title>
</head>
<body>
<div class="content-body">
      <div class="content-wrapper">
 <div class="content-header row">
          <div class="content-header-left col-md-8 col-xs-12">
         
            
          </div>
          
        </div>
		
		<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            
	            <div class="card-body collapse in">
	                <div class="card-block">
							
						
					
	                    <form:form class="form" method="POST" action="${context}/add_update_job_category_master" modelAttribute="jobCategoryDto">
	                     
	                     
	                       
	                    <div class="form-body">
	                    <div class="row">
	                    			<div class="form-group col-md-6 mb-2 ">
			                            <p  class="control-label " >Category Name</p>
			                            
			                             <form:input maxlength="200" type="text" path="categoryName" class="form-control "  placeholder="Category Name"/>
			                            
			                        </div>
			                        </div>
	                    <div class="row">
	                     <div class="col-md-12">
	                     
							
						
							<div class="row">
							<div class="col-sm-1 mb-2">
							<strong>S no.</strong>
							</div>
							<div class="col-sm-3 mb-2">
							<strong> Sub Category Name</strong>
							</div>
							<div class="col-sm-2 mb-2">
							<strong>Action</strong>
							</div>
							</div> 
						
							<div id="table" class="divToAppend ">
							
			          	
							<input type="hidden" id="counter" value="0"/>
							
							<div class="row div0">
							<div class="col-sm-1">
							<strong><p class="sno">1.</p></strong>
							</div>
							
							
							<div class="col-sm-2 mb-2">
							<form:input placeHolder="Sub Category Name" path="subCategoryDtos[0].subCategoryName" data-id="0" class="form-control  category"/>

							</div>
							
							<div class="col-sm-2 mb-2">
							<button type="button" class="addBtn btn btn-sm btn-success">+ Add</button>
							</div>
						
										
							</div>
							
							
							
			          	
							</div>
							 
    
	         		</div>
	                   </div>
	                    
	               </div>
							
							 <div class="form-actions right">
	                          
	                            
	                            
	                             <input type="button" class="btn btn btn-primary " id="saveChanges" value="Next" />
	                             
	                           
	                        </div>
	              
                        
	                    </form:form>
	                </div>
	            </div>
	        </div>
	    </div>
</div>
</div>
	  
	</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
var counter=$('#counter').val();
$(document).ready(function(){
	
	 $("#saveChanges").click(function(){
		
		$("#jobCategoryDto").submit();
		
		});

$(".addBtn").click(function(){

				counter++;
				var appendHtmlString='<div id="table" class="row div'+counter+'"><br>'+
					'<div class="col-sm-1">'+
				'<strong><p class="sno">'+(counter+1)+'.</p></strong>'+
				'</div>'+
				
				'<div class="col-sm-2 mb-2">'+
				'<input id="subCategoryDtos'+counter+'.subCategoryName" data-id="'+counter+'" name="subCategoryDtos['+counter+'].subCategoryName" class="form-control category" placeholder="Sub Category Name" type="text" value="">'+
				'</div>'+
				'<div class="col-sm-2 mb-2">'+
				'<button type="button" data-idr="'+counter+'" class="removeBtn btn btn-sm btn-warning hide'+counter+'">-</button>'+
				'</div>'+
				'</div>';
				$('.divToAppend').append(appendHtmlString);
				postRenderOptions();
				var sno=1;
				$('.sno').each(function(i, obj) {
				    $(this).html("<strong>"+sno+".</strong>");
				    sno++;
				});

			});
postRenderOptions();
function postRenderOptions()
{
	$(".removeBtn").click(function(){

		var counterToManage=$(this).attr('data-idr');
		$('.div'+counterToManage+'').remove();
		var sno=1;
		$('.sno').each(function(i, obj) {
		    $(this).html("<strong>"+sno+".</strong>");
		    sno++;
		});
		

	}); 
}
});
</script>			
</body>
</html>